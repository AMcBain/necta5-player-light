(function ()
{
    "use strict";

    // If the tools script (this file) can be identified, try to grab the query string to
    // use as a unique prefix for saving to local storage, which saves keys per domain,
    // to allow for differentiation of multiple players deployed on the same domain.
    //
    var prefix = (document.querySelector && document.querySelector("[src*='tools']"));
    prefix = (prefix && prefix.src.match(/(?:\?[^?]+)?$/)[0].substring(1)) || "";

    if (prefix)
    {
        prefix += ":";
    }

    var Time = {
        Hours: 3600000,
        Minutes: 60000,
        Seconds: 1000
    };

    // Used with Array.prototype.map, as it gives two arguments so feeding it the text
    // function would always invoke the set before, not the get behavior.
    function textg(node)
    {
        return Tools.text(node);
    }

    window.Tools = {
        // Fuzzy time formatting. It should be easier to see relatively when a song will play:
        // "I need to be back in 5 minutes to hear my request." Plus this also removes confusion
        // over whether the time is in the server's timezone or the local (user's) timezone.
        fuzzytime: function (time, server)
        {
            var hours, minutes, seconds, diff = Math.abs(time - server), before = "", after = "";

            if (time < server)
            {
                after = " ago";
            }
            else
            {
                before = "in ";
            }

            // It is unlikely the queue will ever be > 1 day, so just keep counting up in hours.
            hours = Math.floor(diff / Time.Hours);
            minutes = Math.floor((diff - hours * Time.Hours) / Time.Minutes);
            seconds = Math.floor((diff - minutes * Time.Minutes) / Time.Seconds);

            if (hours)
            {
                return (before + hours + "h" + (minutes ? " " + minutes + "m" : "") + after);
            }
            else if (minutes)
            {
                return (before + minutes + "m" + (seconds ? " " + seconds + "s" : "") + after);
            }
            else if (seconds)
            {
                return (before + seconds + "s" + after);
            }
            return "now";
        },
        // Turns seconds into a duration of the format minutes:seconds.
        duration: function (time)
        {
            var minutes, seconds;

            minutes = Math.floor(time / 60);
            seconds = Math.floor(time - minutes * 60);

            if (seconds < 10)
            {
                seconds = "0" + seconds;
            }

            return (minutes + ":" + seconds);
        },
        link: function (node, value, url)
        {
            var a = document.createElement("a");
            a.href = url;
            Tools.text(a, value);

            node.innerHTML = "";
            node.appendChild(a);
        },
        // If no value is given, return the text value of the given node or array of nodes
        // as a comma separated string. If a value is given, set the value of the node.
        text: function (node, value)
        {
            if (value === undefined)
            {
                if (node instanceof Array)
                {
                    return node.map(textg).join(", ");
                }
                return (node.innerText || node.textContent);
            }
            node.textContent = node.innerText = value;
        },
        // Convert all direct children of the given node to a map, with keys by tag name.
        // Multiple instances of a tag will result in an array of values for that key. If
        // the first child parameter is given, the first child of each direct child is
        // mapped instead of that element.
        findChildren: function (node, firstChild)
        {
            var child, children = {};
            node = node.firstElementChild;

            while (node.nextSibling)
            {
                if (node.tagName)
                {
                    child = (firstChild ? node.firstElementChild : node);

                    if (!children[node.tagName])
                    {
                        children[node.tagName] = child;
                    }
                    else if (children[node.tagName] instanceof Array)
                    {
                        children[node.tagName].push(child);
                    }
                    else
                    {
                        children[node.tagName] = [ children[node.tagName], child ];
                    }
                }
                node = node.nextSibling;
            }

            return children;
        },
        // Handlers map can include keys to functions for success, error, and trap. Success is only
        // called on a response of 200, error otherwise. Trap is called when the success or error
        // functions throw an Error. All requests are asynchronous.
        request: function (handlers, url)
        {
            var request = new XMLHttpRequest();
            request.onreadystatechange = function ()
            {
                if (this.readyState === 4)
                {
                    try
                    {
                        ((this.status === 200 ? handlers.success : handlers.error) || Tools.noop).call(request);
                    }
                    catch (e)
                    {
                        (handlers.trap || Tools.noop)(e);
                    }
                }
            };
            request.open("GET", url, true);
            request.send();

            return request;
        },
        noop: function ()
        {
        },
        // Calling setItem can throw an error in some browsers in certain modes.
        // Allows detection of support beyond existence of the object.
        store: function (key, value)
        {
            var success;

            if ("localStorage" in window)
            {
                try
                {
                    localStorage.setItem(prefix + key, value);
                    success = true;
                }
                catch (e)
                {
                }
            }
            return success;
        },
        // Retrieve a local storage value, returning the default if no value
        // exists or localStorage is not available.
        retrieve: function (key, defaultValue)
        {
            var value = defaultValue;

            if ("localStorage" in window)
            {
                try
                {
                    value = localStorage.getItem(prefix + key) || defaultValue;

                    if (/true|false/.test(value))
                    {
                        value = JSON.parse(value);
                    }
                }
                catch (e)
                {
                }
            }
            return value;
        },
        test: (function ()
        {
            var test = document.createElement("test");

            return function (key, value)
            {
                var exists = (key in test.style);

                if (exists && value)
                {
                    test.style[key] = value;

                    // Only works for values which when returned won't have been transmuted by the browser.
                    // .color = "#fff" -> "rgb(255, 255, 255)"; at least in Firefox.
                    return (test.style[key] === value);
                }
                return exists;
            };
        }()),
        select: function (element, on)
        {
            if (on)
            {
                element.setAttribute("checked", "checked");
            }
            else
            {
                element.removeAttribute("checked");
            }
        },
        // Pad with a non-breaking space space.
        // Negative length pads before, positive after.
        pad: function (value, length)
        {
            var i, pad = Math.abs(length) - (value + "").length;

            for (i = 0; i < pad; i++)
            {
                if (length < 0)
                {
                    value = "\u00A0" + value;
                }
                else
                {
                    value += "\u00A0";
                }
            }
            return value;
        },
        codec: function (type)
        {
            type = type.split("/")[1];
            switch (type)
            {
                case "mpeg": return "mp3";
                case "mp4":  return "aac";
                case "aacp": return "aac+";
            }
            return type;
        }
    };
}());
