window.Urls = {
    Title: "SceneSat Radio",
    // Enables replacement without redeploying the main player. It is recommended these be
    // https to prevent browser security mismatch issues if the player is served via https.
    Queue: "https://scenesat.com/api/quicklist",
    Song: "https://scenesat.com/track/",
    // IDs may not be in "order" and they don't have to be numbers. They're to help saved
    // data find the stream they're associated with across insertion or removal of streams.
    Streams: [
        {
            id: "2",
            country: "normal",
            host: "sentinel",
            src: "http://sentinel.scenesat.com:8000/scenesatmax",
            type: "audio/mpeg",
            rate: 320
        },
        {
            id: "3",
            country: "normal",
            host: "ikonos",
            src: "http://ikonos.scenesat.com:8000/scenesatmax",
            type: "audio/mpeg",
            rate: 320
        },
        {
            id: "4",
            country: "normal",
            host: "oscar",
            src: "http://oscar.scenesat.com:8000/scenesatmax",
            type: "audio/mpeg",
            rate: 320
        },
        //
        {
            id: "5",
            country: "normal",
            host: "salyut",
            src: "http://salyut.scenesat.com:8000/scenesathi",
            type: "audio/aacp",
            rate: 128
        },
        {
            id: "6",
            country: "normal",
            host: "sentinel",
            src: "http://sentinel.scenesat.com:8000/scenesathi",
            type: "audio/aacp",
            rate: 128
        },
        {
            id: "7",
            country: "normal",
            host: "ikonos",
            src: "http://ikonos.scenesat.com:8000/scenesathi",
            type: "audio/aacp",
            rate: 128
        },
        {
            id: "8",
            country: "normal",
            host: "oscar",
            src: "http://r4.scenesat.com:8000/scenesathi",
            type: "audio/aacp",
            rate: 128
        },
        //
        {
            id: "10",
            country: "normal",
            host: "salyut",
            src: "http://salyut.scenesat.com:8000/scenesat",
            type: "audio/mpeg",
            rate: 128
        },
        {
            id: "11",
            country: "normal",
            host: "sj-1",
            src: "http://sc.scenesat.com:8000/scenesat",
            type: "audio/mpeg",
            rate: 128
        },
        {
            id: "12",
            country: "normal",
            host: "sentinel",
            src: "http://sentinel.scenesat.com:8000/scenesat",
            type: "audio/mpeg",
            rate: 128
        },
        {
            id: "13",
            country: "normal",
            host: "oscar",
            src: "http://oscar.scenesat.com:8000/scenesat",
            type: "audio/mpeg",
            rate: 128
        },
        //
        {
            id: "14",
            country: "normal",
            host: "sentinel",
            src: "http://sentinel.scenesat.com:8000/scenesatmed",
            type: "audio/aacp",
            rate: 48
        },
        {
            id: "16",
            country: "normal",
            host: "ikonos",
            src: "http://ikonos.scenesat.com:8000/scenesatmed",
            type: "audio/aacp",
            rate: 48
        },
        {
            id: "17",
            country: "normal",
            host: "oscar",
            src: "http://oscar.scenesat.com:8000/scenesatmed",
            type: "audio/aacp",
            rate: 48
        },
        //
        {
            id: "21",
            country: "firewalled",
            host: "salyut",
            src: "http://salyut80.scenesat.com:80/scenesatmax",
            type: "audio/mpeg",
            rate: 320
        },
        {
            id: "22",
            country: "firewalled",
            host: "salyut",
            src: "http://salyut80.scenesat.com:80/scenesathi",
            type: "audio/aacp",
            rate: 128
        },
        {
            id: "23",
            country: "firewalled",
            host: "salyut",
            src: "http://salyut80.scenesat.com:80/scenesat",
            type: "audio/mpeg",
            rate: 128
        },
        {
            id: "25",
            country: "firewalled",
            host: "salyut",
            src: "http://salyut80.scenesat.com:80/scenesatmed",
            type: "audio/aacp",
            rate: 48
        }
    ]
};
