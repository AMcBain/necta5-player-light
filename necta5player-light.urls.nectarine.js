window.Urls = {
    Title: "Nectarine",
    Subtitle: "Demoscene Radio",
    // Enables replacement without redeploying the main player. It is recommended these be
    // https to prevent browser security mismatch issues if the player is served via https.
    Queue: "https://radios.asmcbain.net/nectarine/xml/queue/",
    Oneliner: "https://radios.asmcbain.net/nectarine/xml/oneliner/",
    Song: "https://www.scenemusic.net/demovibes/song/",
    // IDs may not be in "order" and they don't have to be numbers. They're to help saved
    // data find the stream they're associated with across insertion or removal of streams.
    Streams: [
        {
            id: "20",
            country: "DE",
            host: "amigaman",
            src: "http://nectarine.from-de.com/necta192",
            type: "audio/mpeg",
            rate: 192
        },
        {
            id: "21",
            country: "FR",
            host: "minus",
            src: "http://necta-relay.mnus.de:8000/necta192.mp3",
            type: "audio/mpeg",
            rate: 192
        },
        {
            id: "22",
            country: "NO",
            host: "teddy",
            src: "http://no.scenemusic.net:9000/necta",
            type: "audio/mpeg",
            rate: 192
        },
        {
            id: "23",
            country: "UK",
            host: "jansenra",
            src: "http://necta.jansenit.com:8000/necta192.mp3",
            type: "audio/mpeg",
            rate: 192
        }/*,
        {
            id: "1",
            country: "DE",
            host: "Locutus",
            src: "http://de.scenemusic.net/necta128.aac",
            type: "audio/mp4",
            rate: 128
        },
        {
            id: "2",
            country: "DE",
            host: "Locutus",
            src: "http://de.scenemusic.net/necta80.aac",
            type: "audio/mp4",
            rate: 80
        },
        {
            id: "3",
            country: "DE",
            host: "Locutus",
            src: "http://de.scenemusic.net/necta48.aac",
            type: "audio/mp4",
            rate: 48
        },
        {
            id: "4",
            country: "DE",
            host: "Locutus",
            src: "http://de.scenemusic.net/necta16.aac",
            type: "audio/mp4",
            rate: 16
        },
        {
            id: "5",
            country: "DE",
            host: "Locutus",
            src: "http://de.scenemusic.net/necta192.mp3",
            type: "audio/mpeg",
            rate: 192
        },
        {
            id: "6",
            country: "NL",
            host: "qk3",
            src: "http://nectarine.q3k.org:1234/necta192",
            type: "audio/mpeg",
            rate: 192
        },
        {
            id: "7",
            country: "SE",
            host: "djrandom",
            src: "http://se.scenemusic.net:8000/necta128.aac",
            type: "audio/mp4",
            rate: 128
        },
        {
            id: "8",
            country: "SE",
            host: "djrandom",
            src: "http://se.scenemusic.net:8000/necta192.mp3",
            type: "audio/mp3",
            rate: 192
        }*/,
        {
            id: "9",
            country: "US",
            host: "unfy",
            src: "http://scenemusic.unfy.org/necta",
            type: "audio/mpeg",
            rate: 192
        },
        {
            id: "11",
            country: "US",
            host: "unfy",
            src: "http://scenemusic.unfy.org/necta64",
            type: "audio/mpeg",
            rate: 64
        }/*,
        {
            id: "10.1",
            country: "US",
            host: "unfy",
            src: "http://scenemusic.unfy.org/necta128.ogg",
            type: "audio/ogg",
            rate: 128
        }*/,
        {
            id: "10",
            country: "US",
            host: "unfy",
            src: "http://scenemusic.unfy.org/necta64.ogg",
            type: "audio/ogg",
            rate: 64
        }/*,
        {
            id: "14",
            country: "DE",
            host: "maep/minus",
            src: "http://hyperion.mnus.de:8000/necta96.opus",
            type: "audio/ogg",
            rate: 98
        },
        {
            id: "13",
            country: "DE",
            host: "maep/minus",
            src: "http://hyperion.mnus.de:8000/necta64.opus",
            type: "audio/ogg",
            rate: 64
        },
        {
            id: "12",
            country: "DE",
            host: "maep/minus",
            src: "http://hyperion.mnus.de:8000/necta24.opus",
            type: "audio/ogg",
            rate: 24
        }*/
    ]
};
