window.Urls = {
    Title: "CVGM Radio",
    // Enables replacement without redeploying the main player. It is recommended these be
    // https to prevent browser security mismatch issues if the player is served via https.
    Queue: "https://radios.asmcbain.net/cvgm/xml/queue/",
    Oneliner: "https://radios.asmcbain.net/cvgm/xml/oneliner/",
    Song: "https://www.cvgm.net/demovibes/song/",
    // IDs may not be in "order" and they don't have to be numbers. They're to help saved
    // data find the stream they're associated with across insertion or removal of streams.
    Streams: [
        {
            id: "5",
            country: "DE",
            host: "Locutus",
            src: "http://de.cvgm.net/cvgm80.aac",
            type: "audio/aacp",
            rate: 80
        },
        {
            id: "4",
            country: "DE",
            host: "Locutus",
            src: "http://de.cvgm.net/cvgm48.aac",
            type: "audio/aacp",
            rate: 48
        },
        {
            id: "3",
            country: "DE",
            host: "Locutus",
            src: "http://de.cvgm.net/cvgm16.aac",
            type: "audio/aacp",
            rate: 16
        },
        {
            id: "1",
            country: "DE",
            host: "Locutus",
            src: "http://de.cvgm.net/cvgm128.mp3",
            type: "audio/mpeg",
            rate: 128
        },
        {
            id: "2",
            country: "US",
            host: "CVGM",
            src: "http://slacker.cvgm.net:8000/cvgm128",
            type: "audio/mpeg",
            rate: 128
        },
    ]
};
