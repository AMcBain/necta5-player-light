(function ()
{
    "use strict";

    window.onload = function ()
    {
        if (!sorry())
        {
            setupStyles();
            setupControls();
            warning.style.display = (Tools.store("test", "Test") ? "" : "block");

            stopUpdates();
        }
    };

    function stopUpdates ()
    {
        Data.stopUpdates();

        Tools.text(title, Urls.Title || "Necta5 Player Light");
        Tools.text(artist, Urls.Subtitle || "");
        datapanel.innerHTML = "";
    }

    function setupStyles ()
    {
        // Yes, yes, evil and all that stuff.
        // Nothing to see here, move along.
        var agent = navigator.userAgent;

        if (agent.indexOf("Firefox") > -1)
        {
            player.style.background = "black";
            player.parentNode.style.background = "#1a1718";
            preferences.style.background = "#1a1718";
        }
        else if (agent.indexOf("MSIE") > -1)
        {
            player.parentNode.style.background = "black";
            preferences.style.background = "black";
        }
        else if (agent.indexOf("WebKit") > -1)
        {
            options.className = "webkit";
        }

        // Update button ideograms 
        var button;
        if (Tools.test("fontFamily", "FontAwesome"))
        {
            button = document.querySelector("[title='preferences']");
            button.innerHTML = "\uf022";
            button.style.font = "normal 20px/28px FontAwesome";

            button = document.querySelector("[title='close']");
            button.innerHTML = "\uf021";
            button.style.font = "normal 20px/28px FontAwesome";
        }
    }

    function flipZIndex (a, b)
    {
        widget.firstElementChild.style.zIndex = a;
        widget.lastElementChild.style.zIndex = b;
    }

    function setupControls ()
    {
        var i, item, option, changing, updateOnClose, selected = Tools.retrieve("stream");

        // If no animation support, flip z-index of children.
        document.querySelector("[title='preferences']").addEventListener("click", function ()
        {
            widget.className = "preferences";
            flipZIndex(0, 1);
        });
        document.querySelector("[title='close']").addEventListener("click", function ()
        {
            widget.className = "";
            flipZIndex(1, 0);

            if (updateOnClose)
            {
                Data.quickUpdate();
                updateOnClose = false;
            }
        });

        player.addEventListener("play", function()
        {
            Data.startUpdates();
        });
        player.addEventListener("pause", function()
        {
            changing = true;

            // Kill the buffer. IE10 seemed to want to keep buffering on save
            // making the stream get really far behind where it should be.
            player.src = player.src;

            stopUpdates();
        });
        player.addEventListener("error", function()
        {
            player.pause();
            Tools.text(title, "Stream error");
            Tools.text(artist, "Stream failed to load");
        });
        player.addEventListener("emptied", function()
        {
            if (!changing)
            {
                player.pause();
                Tools.text(title, "Stream error");
                Tools.text(artist, "Server connection lost");
            }
            changing = false;
        });

        for (i = 0; i < Urls.Streams.length; i++)
        {
            item = Urls.Streams[i];

            if (player.canPlayType(item.type))
            {
                option = document.createElement("li");
                option.setAttribute("data-id", item.id);
                option.setAttribute("data-streams-index", i);
                option.setAttribute("data-dom-index", stream.children.length);
                // These "read" better with the span last, but because the span is floated, WebKit has issues rendering it. It determines
                // the width is the length of the second longest line, so the longest line is forced to push its float to the next line.
                option.innerHTML = "<span>" + item.rate + " kbps / " + Tools.codec(item.type) + "</span> " + item.country + " " + item.host;
                stream.appendChild(option);

                if (item.id === selected)
                {
                    selected = option;
                }
            }
        }

        function setStream (target)
        {
            stream.firstElementChild.style.marginTop = -parseInt(target.getAttribute("data-dom-index")) * target.clientHeight + "px";

            var playing = !player.paused;
            if (playing)
            {
                // Need to do?
                player.pause();
            }
            else
            {
                stopUpdates();
            }

            changing = true;
            player.src = Urls.Streams[target.getAttribute("data-streams-index")].src;

            // Firefox hides the player when the stream can't be loaded. It doesn't
            // make it reappear when a valid stream is loaded later. This fixes that.
            player.style.display = "none";
            player.style.display = "";

            if (playing)
            {
                player.play();
            }
        }
        // Chrome seemed to sometimes get the wrong offset for the default (or stored) selected stream.
        // This forces it to wait to do that until all has settled and has proper sizes.
        setTimeout((function (selected)
        {
            return function ()
            {
                setStream(selected && selected.tagName ? selected : stream.firstElementChild);
            };
        }(selected)), 1000);

        stream.addEventListener("click", function (event)
        {
            var target = event.target || event.srcElement;

            if (target.tagName !== "LI")
            {
                target = target.parentNode;
            }

            if (stream.className)
            {
                stream.className = "";
                setStream(target);

                Tools.store("stream", target.getAttribute("data-id"));
            }
            else
            {
                stream.className = "open";
            }

            if (event.stopPropagation())
            {
                event.stopPropagation();
            }
            event.cancelBubble = true;
        });

        document.body.addEventListener("click", function ()
        {
            stream.className = "";
        });

        function change (event)
        {
            var target = event.target || event.srcElement;

            if (target.checked)
            {
                Tools.store(selected.value, selected.checked);
                Tools.store(target.value, target.checked);
                selected = target;
            }

            updateOnClose = !player.paused;
        }

        if (Data.Options && Data.Options.length)
        {
            // Leaky, but I don't care.
            Data.Options.unshift({
                name: "nothing",
                label: "Nothing"
            });

            for (i = 0; i < Data.Options.length; i++)
            {
                option = document.createElement("li");;

                item = document.createElement("input");
                item.id = Data.Options[i].name;
                item.type = "radio";
                item.name = "panel";
                item.value = Data.Options[i].name;
                item.addEventListener("change", change);
                option.appendChild(item);

                // Restore user choice and detect which one was selected.
                Tools.select(item, Tools.retrieve(Data.Options[i].name, i === 0));

                if (item.checked)
                {
                    selected = item;
                }

                item = document.createElement("label");
                item.htmlFor = Data.Options[i].name;
                item.innerHTML = Data.Options[i].label;
                option.appendChild(item);

                options.appendChild(option);
            }
        }
        else
        {
            // No panel options.
            options.lastElementChild.style.display = "none";
        }
    }

    // You can't always get what you want.
    function sorry ()
    {
        var i, message;

        if (!("canPlayType" in player))
        {
            message = "This browser doesn't support the audio tag. Your browser is holding you back, man!";
        }

        // TODO done twice: here and when building the stream selector. Reorganize to de-dupe?
        if (!message)
        {
            for (i = 0; i < Urls.Streams.length; i++)
            {
                if (player.canPlayType(Urls.Streams[i].type))
                {
                    break;
                }
            }

            if (i >= Urls.Streams.length || location.search === "?StreamsErr")
            {
                message = "This browser doesn't support any of the available stream formats.";
            }
        }

        if (!("XMLHttpRequest" in window) || location.search === "?XMLHttpRequestErr")
        {
            message = "\"XMLHttpRequest\" support is required here. No ifs, ands, or buts about it!";
        }

        if (!("DOMParser" in window) || location.search === "?DOMParserErr")
        {
            message = "No \"DOMParser\" support? No widget for you!";
        }

        if (!("querySelector" in document) || location.search === "?querySelectorErr")
        {
            message = "Support for \"querySelector\" is missing. Return when you've some of that.";
        }

        if (!("firstElementChild" in document.body) || location.search === "?firstElementChildErr")
        {
            message = "Woe be to you who uses a browser that doesn't support \"firstElementChild\".";
        }

        if (!Array.prototype.map || location.search === "?Array.prototype.mapErr")
        {
            message = "This browser is behind on its JS features. Array.prototype.map support missing.";
        }

        if (!("JSON" in window) || location.search === "?JSONErr")
        {
            message = "All the cool browsers support native JSON. You should get one that does.";
        }

        if (!Array.prototype.unshift || location.search === "?unshiftErr")
        {
            message = "Couldn't find Array.prototype.unshift; Very surprised to find it missing.";
        }

        if (message)
        {
            message = "<h1>Problem</h1><p>" + message + "</p>";

            widget.className = "sorry";
            widget.children[0].children[widget.children[0].children.length - 1].innerHTML = message;
        }
        return !!message;
    }
}());
