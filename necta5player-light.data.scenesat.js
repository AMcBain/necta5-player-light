(function Data ()
{
    "use strict";
    var lastTimeout, lastQueueXhr, lastTimer, lastServerTime, times = [];

    lastQueueXhr = {
        abort: Tools.noop
    };

    function handlers (success)
    {
        return {
            success: function()
            {
                var response, server;
                response = JSON.parse(this.responseText).tunes;
                server = new Date(this.getResponseHeader("Date")).getTime();
                success.call(this, response, server);
            },
            error: function ()
            {
                Tools.text(title, "bad http status");
                Tools.text(artist, this.responseText);
            },
            trap: function (e)
            {
                Tools.text(title, "javascript error");
                Tools.text(artist, e.message);
            }
        };
    }

    function updatePanel (response, server)
    {
        var start;

        lastServerTime = server;
        datapanel.innerHTML = "";
        times = [];

        if (response.other.islive)
        {
            live(response.live);
        }
        else
        {
            start = new Date(response.next.startingfull);
            times.push(song(response.next, server, start));
        }

        // In theory this should exist during a live show, but it seems it only shows up somtimes.
        if (response.previous.id)
        {
            start = response.now.startedfull;
            start = (start.getSeconds && start) || new Date(start);
            start.setSeconds(start.getSeconds() - Number(response.previous.duration));
            times.push(song(response.previous, server, start, true));
        }

        metadata(response.other);
    }

    function live (data)
    {
        var title, show, host, item;

        title = document.createElement("h3");
        Tools.text(title, "Live Show");

        show = document.createElement("h4");
        Tools.text(show, data.name);

        host = document.createElement("h5");
        Tools.text(host, data.host);

        item = document.createElement("div");
        item.appendChild(title);
        item.appendChild(show);
        item.appendChild(host);

        datapanel.appendChild(item);
    }

    function song (data, server, playstart, historical)
    {
        var title, item, song, artist, date, time, length;

        title = document.createElement("h3");
        Tools.text(title, historical ? "Last Played" : "Next Up");

        time = document.createElement("time");
        time.setAttribute("datetime", playstart.toISOString());
        time.setAttribute("data-time", playstart.getTime());
        Tools.text(time, Tools.fuzzytime(playstart, server));

        song = document.createElement("h4");
        Tools.text(song, data.title);

        if (!historical)
        {
            length = document.createElement("div");
            Tools.text(length, Tools.duration(data.duration));
        }

        artist = document.createElement("h5");
        Tools.text(artist, data.artist);

        item = document.createElement("div");
        item.appendChild(title);
        item.appendChild(time);
        item.appendChild(song);

        if (length)
        {
            item.appendChild(length);
        }
        item.appendChild(artist);

        datapanel.appendChild(item);
        return time;
    }

    function metadata (data)
    {
        var item, msg;

        msg = data.listeners + " ";
        msg += (data.listeners === 1 ? "listener" : "listeners");

        if (data.countries > 1)
        {
            msg += " across " + data.countries + " countries";
        }

        item = document.createElement("span");
        Tools.text(item, msg);
        datapanel.appendChild(item);
    }

    function songtime ()
    {
        var i, time;
        for (i = 0; i < times.length; i++)
        {
            time = Number(times[i].getAttribute("data-time")) - 1000;
            times[i].setAttribute("datetime", new Date(time).toISOString());
            times[i].setAttribute("data-time", time);
            Tools.text(times[i], Tools.fuzzytime(time, lastServerTime));
        }
    }

    window.Data = {
        // Name is the ID given to the element and used when storing whether it is selected.
        // Label is what id displayed in the preferences panel. Order matters. If no options
        // are given none are shown, but if there's at least one option a default "nothing"
        // option is generated.
        Options: [ {
            name: "quicklist",
            label: "Quicklist",
        } ],
        stopUpdates: function ()
        {
            lastQueueXhr.abort();
            clearTimeout(lastTimeout);
            clearInterval(lastTimer);
        },
        startUpdates: function (response, server)
        {
            clearTimeout(lastTimeout);
            clearInterval(lastTimer);

            lastTimer = setInterval(songtime, 1000);

            var queueHandlers = handlers(function (response, server)
            {
                var now, time;

                if (!response.other || !response.live || !response.previous || !response.now || !response.next)
                {
                    Tools.text(title, "Bad data error");
                    Tools.text(artist, "Response is incomplete");
                }
                else
                {
                    now = response.now;
                    now.startedfull = new Date(now.startedfull);

                    // Temporary fix; I don't like it, but the server sometimes doesn't provide startedfull when it does provide startedat.
                    // Splitting startedat, applying it to Date, then appending a fixed timezone to the resulting string mixes timezones in
                    // a bad way and IE will always return a string representation of now when doing that. So instead this will cause the a
                    // hop that is equal to the song length which in theory shouldn't be too bad assuming there's not a lot of update drift.
                    if (isNaN(now.startedfull))
                    {
                        now.startedfull = new Date(server);
                    }

                    // TODO: hang around during a live show to see if this is possible and tweak the value (and comment here) if so.
                    if (isNaN(Number(now.duration)))
                    {
                        now.duration = 60;
                    }

                    // Figure out how long the song is, how much time has been spent playing it, and how much time is left until it is done.
                    // The client's time can't be trusted, so the server's is used instead. This allows timeouts to be set so that the return
                    // just after the current song ends rather than sloppy timed intervals. The only monkey with a wrench is jingles. Jingles
                    // are of an unknown time length and the next song doesn't start until the jingle ends. Until then, the current song will
                    // have appeared to run longer than the listed length. Thus the next request after a jingle will be extremely late. But
                    // jingles only happen once every half hour or so, which makes this less bad.
                    now.duration = now.duration * 1000;
                    time = now.duration - (server - now.startedfull);

                    // Must be a jingle? Do SceneSat jingles stop the update of the stream data until finished?
                    if (time <= 0 || now.title === title.innerHTML)
                    {
                        // SceneSat jingles don't seem to be very long.
                        lastTimeout = setTimeout(get, 10000);
                    }
                    else
                    {
                        // Get going again as soon as possible.
                        lastTimeout = setTimeout(get, time + 5000);

                        Tools.text(title, now.title, Urls.Song + now.id);
                        Tools.text(artist, now.artist);

                        if (quicklist.checked)
                        {
                            updatePanel(response, server);
                        }
                    }
                }
            });

            function get ()
            {
                lastQueueXhr.abort();
                lastQueueXhr = Tools.request(queueHandlers, Urls.Queue);
            }

            lastTimeout = setTimeout(get, 1000);
        },
        // Called when a change is made which might be able to be handled without making a heavier data request.
        quickUpdate: function ()
        {
            if (nothing.checked)
            {
                datapanel.innerHTML = "";
            }
            else
            {
                lastQueueXhr.abort();
                lastQueueXhr = Tools.request(handlers(updatePanel), Urls.Queue);
            }
        }
    };
}());
