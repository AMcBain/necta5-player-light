(function Data ()
{
    "use strict";
    var lastTimeout, lastQueueXhr, lastOnelinerXhr;

    lastQueueXhr = {
        abort: Tools.noop
    };
    lastOnelinerXhr = lastQueueXhr;

    function handlers (success, firstChild)
    {
        if (arguments.length === 1)
        {
            firstChild = true;
        }
        return {
            success: function()
            {
                var response, server;
                response = new DOMParser().parseFromString(this.responseText, "text/xml").firstChild;
                server = new Date(this.getResponseHeader("Date")).getTime();
                success.call(this, Tools.findChildren(response, firstChild), server);
            },
            error: function ()
            {
                Tools.text(title, "bad http status");
                Tools.text(artist, this.responseText);
            },
            trap: function (e)
            {
                Tools.text(title, "javascript error");
                Tools.text(artist, e.message);
            }
        };
    }

    function updatePanel (response, server)
    {
        var i, item, historical, process = song;

        if (oneliner.checked)
        {
            item = response.entry[0];
            process = message;
        }
        else if (queue.checked)
        {
            item = response.queue;
        }
        else if (past.checked)
        {
            item = response.history;
            historical = true;
        }
        datapanel.innerHTML = "";

        i = 0;
        while (item && i < 4)
        {
            if (item.nodeType !== 3)
            {
                datapanel.appendChild(process(item, server, historical));
                i++;
            }
            item = item.nextSibling;
        }
    }

    function song (data, server, historical)
    {
        var item, song, artist, date, time, length;

        data = Tools.findChildren(data);
        item = document.createElement("div");

        date = new Date(Tools.text(data.playstart));
        time = document.createElement("time");
        time.setAttribute("datetime", date.toISOString());
        Tools.text(time, Tools.fuzzytime(date, server));
        item.appendChild(time);

        song = document.createElement("h4");
        Tools.text(song, Tools.text(data.song));
        item.appendChild(song);

        if (!historical)
        {
            length = document.createElement("div");
            Tools.text(length, data.song.getAttribute("length"));
            item.appendChild(length);
        }

        artist = document.createElement("h5");
        Tools.text(artist, Tools.text(data.artist));
        item.appendChild(artist);

        return item;
    }

    function message (data, server)
    {
        var item, author, msg, date, time = data.getAttribute("time");

        data = Tools.findChildren(data);
        item = document.createElement("div");

        date = new Date(time);
        time = document.createElement("time");
        time.setAttribute("datetime", date.toISOString());
        Tools.text(time, Tools.fuzzytime(date, server));
        item.appendChild(time);

        author = document.createElement("h4");
        Tools.text(author, Tools.text(data.author));
        item.appendChild(author);

        msg = document.createElement("p");
        Tools.text(msg, Tools.text(data.message));
        item.appendChild(msg);

        return item;
    }

    window.Data = {
        // Name is the ID given to the element and used when storing whether it is selected.
        // Label is what id displayed in the preferences panel. Order matters. If no options
        // are given none are shown, but if there's at least one option a default "nothing"
        // option is generated.
        Options: [ {
            name: "oneliner",
            label: "Oneliner",
        }, {
            name: "queue",
            label: "Pending Queue"
        }, {
            name: "past",
            label: "Request History"
        } ],
        stopUpdates: function ()
        {
            lastQueueXhr.abort();
            lastOnelinerXhr.abort();
            clearTimeout(lastTimeout);
        },
        startUpdates: function (response, server)
        {
            clearTimeout(lastTimeout);

            var onelinerHandlers, queueHandlers = handlers(function (response, server)
            {
                var now, time;

                if (!response.now || !response.history)
                {
                    Tools.text(title, "Bad data error");
                    Tools.text(artist, "Missing now or history");
                }
                else
                {
                    now = Tools.findChildren(response.now);
                    now.playstart = new Date(now.playstart.innerText || now.playstart.textContent);

                    // Figure out how long the song is, how much time has been spent playing it, and how much time is left until it is done.
                    // The client's time can't be trusted, so the server's is used instead. This allows timeouts to be set so that the return
                    // just after the current song ends rather than sloppy timed intervals. The only monkey with a wrench is jingles. Jingles
                    // are of an unknown time length and the next song doesn't start until the jingle ends. Until then, the current song will
                    // have appeared to run longer than the listed length. Thus the next request after a jingle will be extremely late. But
                    // jingles only happen once every half hour or so, which makes this less bad. Using the playstart value of the next song
                    // in the queue would work instead of this mess but cannot be used due to the possibility of the queue being empty.
                    now.length = now.song.getAttribute("length").split(":");
                    now.length = (Number(now.length[0]) * 60 + Number(now.length[1])) * 1000;
                    time = now.length - (server - now.playstart);

                    // Must be a jingle?
                    if (time <= 0 || Tools.text(now.song) === title.innerHTML)
                    {
                        // Longest jingle in the database is 38s. Techinically this could set a shorter duration to try to keep "hopping"
                        // until the next song is played, but that doesn't help the app keep a low request interval profile.
                        lastTimeout = setTimeout(get, 40000);
                    }
                    else
                    {
                        // Get going again as soon as possible.
                        lastTimeout = setTimeout(get, time + 5000);

                        Tools.text(title, Tools.text(now.song), Urls.Song + now.song.getAttribute("id"));
                        Tools.text(artist, Tools.text(now.artist));

                        if (queue.checked || past.checked)
                        {
                            updatePanel(response, server);
                        }
                    }
                }
            });
            onelinerHandlers = handlers(updatePanel, false);

            function get ()
            {
                lastQueueXhr.abort();
                lastQueueXhr = Tools.request(queueHandlers, Urls.Queue);

                if (oneliner.checked)
                {
                    lastOnelinerXhr.abort();
                    lastOnelinerXhr = Tools.request(onelinerHandlers, Urls.Oneliner);
                }
            }

            lastTimeout = setTimeout(get, 1000);
        },
        // Called when a change is made which might be able to be handled without making a heavier data request.
        quickUpdate: function ()
        {
            if (nothing.checked)
            {
                datapanel.innerHTML = "";
            }
            else if (oneliner.checked)
            {
                lastOnelinerXhr.abort();
                lastOnelinerXhr = Tools.request(handlers(updatePanel, false), Urls.Oneliner);
            }
            else
            {
                lastQueueXhr.abort();
                lastQueueXhr = Tools.request(handlers(updatePanel), Urls.Queue);
            }
        }
    };
}());
